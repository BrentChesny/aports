# Contributor: rubicon <rubicon@mailo.com>
# Maintainer:
pkgname=ocaml-down
_pkgname=down
pkgver=0.1.0
pkgrel=0
pkgdesc="OCaml toplevel (REPL) upgrade"
url="https://erratique.ch/software/down"
# limited by riscv64: ocaml, ppc64le: ocaml-uucp
arch="all !riscv64 !ppc64le"
license="ISC"
depends="ocaml-runtime"
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="
	ocaml
	ocaml-compiler-libs
	ocaml-findlib
	ocamlbuild
	ocaml-topkg
	ocaml-uucp-dev
	cmd:opam-installer
	"
options="!check"  # no tests provided
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.bz2::https://erratique.ch/software/down/releases/down-$pkgver.tbz"
builddir="$srcdir/$_pkgname-$pkgver"
_ocamldir=usr/lib/ocaml

# 32-bit archs
case "$CARCH" in
	arm*|x86) options="$options textrels" ;;
esac

build() {
	ocaml pkg/pkg.ml build \
		--lib-dir "$(ocamlc -where)"
}

package() {
	opam-installer -i \
		--prefix="$pkgdir/usr" \
		--libdir="$pkgdir/$_ocamldir" \
		--docdir="$builddir/.omit" \
		$_pkgname.install
}

dev() {
	default_dev

	cd "$pkgdir"

	local path; for path in $(find $_ocamldir \( \
			-name '*.cmt' -o \
			-name '*.cmti' -o \
			-name '*.cmx' -o \
			-name '*.cmxa' -o \
			-name '*.ml' -o \
			-name '*.mli' \
		\))
	do
		amove "$path"
	done
}

sha512sums="
f09d34f9a21d6a65ce85d9f6c267ee811d85cc0a91ab9dd76ed6ca0e8183df09a2e77331484e54661d154465d64fc31adb4be4a94615ef735c772d56b409a9e3  ocaml-down-0.1.0.tar.bz2
"
