# Contributor: rubicon <rubicon@mailo.com>
# Maintainer:
pkgname=ocaml-mew_vi
_pkgname=mew_vi
pkgver=0.5.0
pkgrel=0
pkgdesc="Modal editing witch (VI interpreter)"
url="https://github.com/kandu/mew_vi"
arch="all !riscv64" # restricted by ocaml
license="MIT"
depends="ocaml-runtime"
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="
	dune
	ocaml
	ocaml-mew-dev
	ocaml-react-dev
	ocaml-result-dev
	ocaml-trie-dev
	"
options="!check"    # needs ppx_expect
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/kandu/mew_vi/archive/$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

# 32-bit archs
case "$CARCH" in
	arm*|x86) options="$options textrels" ;;
esac

build() {
	export OCAMLPATH=/usr/lib/ocaml
	dune build --root . @install --no-buffer --verbose
}

check() {
	dune runtest --no-buffer --verbose
}

package() {
	dune install \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--libdir=/usr/lib/ocaml

	rm -Rf "$pkgdir"/usr/doc
}

dev() {
	default_dev

	cd "$pkgdir"

	local path; for path in $(find usr/lib/ocaml \( \
			-name '*.cmt' -o \
			-name '*.cmti' -o \
			-name '*.cmx' -o \
			-name '*.cmxa' -o \
			-name '*.ml' -o \
			-name '*.mli' \
		\))
	do
		amove "$path"
	done
}

sha512sums="
f6ee1375ceee60ccae1799d07a8bc55684fdbffc2275147ef19cf3c7d242663764e6630b9423287a78efacba17f410971e3fc397d202effb331f94dc00797eb0  ocaml-mew_vi-0.5.0.tar.gz
"
